Docker image for CI deploy
==========================

This image 

- is based on Ubuntu `artful`
- has a Kerboros client
- has a utility to copy to EOS
- has full TeXLive installation
- Doxygen (including graphviz)

Images
------
 
- Base image. Refer to this image as 

		gitlab-registry.cern.ch/cholm/docker-ubuntu-doxygen
		
  This image is based on CentOS 7, has XRootD client, Kerberos,
  and a tool to deploy on EOS. 
  
Usage in other Docker images or deployment
------------------------------------------

These images are intended as base images for other images, or for
deploying software and documentation in GitLab's CI.  The base image
is mostly suitable as base image for other images, while the full
image is more for deploying software. 

### As a base image 

In your `Dockerfile` recipe, do 

	FROM gitlab-registry.cern.ch/cholm/docker-ubuntu-doxygen:base
	
### As a deploy image 

In your `.gitlab-ci.yml` do 

	image gitlab-registry.cern.ch/cholm/docker-ubuntu-doxygen

Standalone usage
----------------

You may also want to use these images for standalone use - i.e., to
execute ROOT in a container.  Do 

	xhost +local && \
		docker run -ti \
			--env=DISPLAY \
			--volume=/tmp/.X11-unix:/tmp/.X11-unix \
			gitlab-registry.cern.ch/cholm/docker-ubuntu-doxygen:base \
			bash 
			

Christian


